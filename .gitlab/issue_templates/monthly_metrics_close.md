# Monthly Metrics Closure Checklist

This issue serves as a checklist for gathering and updating our Monthly Metrics. 

## Important Dates

2nd of the month - Sales closes previous months reconciliation 
3rd of the month - Accounting verifies Sales reconciliation
4th of the month - Sales meets with team to notify changes
5th of the month - Sales sends sales metrics to FinOps. FinOps updates Monthly Metrics sheet, Corporate Dashboard and Investor Update


## Initial Tasks
* [ ] SalesOps notifies FinOps sales metrics are completed
* [ ] Accounting provides initial revenue & cash flow to FinOps 
* [ ] FinOps adds sales and other operating metrics to Financial Model
* [ ] FinOps ensures the forecasted metrics in Revenue Model are updated
* [ ] FinOps transfers `Metrics` data to Investor sheet and follows Investor Update handbook instructions
* [ ] FinOps updates Investor Update document
* [ ] FinOps updates Corporate Dashboard using `SheetLoad` from historical.metrics data and historical.headcount data
* [ ] FinOps sends updated metrics and investor update to CFO for approval
* [ ] Update GitLab.com Subscription Users by 7th of each month
* [ ] Lock down current month GitLab.com model to ensure accuracy and auditablity 
* [ ] Add Trueups to Revenue Model 
* [ ] Update Planned vs Actuals GitLabber sheet & notify PeopleOps

## Closing Tasks
* [ ] Load previous months actuals into models
* [ ] Load ProServe actuals into ProServe model and notify Manager, Professional Services
* [ ] Notify CFO that the actuals are loaded
* [ ] Update Closing Checklist Sheet for Accounting
* [ ] Customer Service Bonus Review - every 3 month
* [ ] Add actuals for Infra, Support and Hosting to .com Model and notify the PM
* [ ] Review B vs A and distribute to department and function leads
* [ ] Update Forecast / Acutal in the Financial Models (Balance Sheet, IS, CFS, Revenue Model)

