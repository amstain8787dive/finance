## Vendor Contract Process

This templated issue serves as a base for routing vendor contracts through the GitLab approval workflow. Remove any options that do not apply to your contract. Please also remove instructions once you have completed them.

### Mark this issue as confidential.

#### PURPOSE
<details open>
<summary>PURPOSE</summary>
<br>

I am requesting a: 

 - [ ] New 
 - [ ] Updated / Trueup / Amended
 - [ ] Renewal 
 - [ ] Termination 

contract with **(VENDOR NAME)**. The vendor’s contract is attached to this issue. [attach contract]
</details>

*****

#### DETAILS
<details open>
<summary>DETAILS</summary>
<br>

This is an agreement for purchase of/license for: (choose applicable items and remove others)

 - [ ] Services 
 - [ ] Products 
 - [ ] Other __________________ 


Please describe the purpose of the contract in detail:

##### Financial 
Is this a:
- [ ] Prepaid Expense [ex: one time marketing events]
* What Month/Year does GitLab incur the expense?  __________________ 

**OR**

- [ ] Recurring [ex: monthly software charges]
* What is the contract start date?  __________________ 

* What is the Total Cost:


##### Marketing
* Estimated SAOs:
* Projected pipeline creation: 

</details>

****

#### CONTACT INFORMATION
<details open>
<summary>CONTACT INFORMATION</summary>
<br>

###### Requester

Your Name: 

Email: 

Department: 

Date Submitted: 

Date Needed (Please provide any special circumstances if requesting an expedited review): 

###### Counterparty 

Company Name: 

Address:

City:

State: 

Country:

Postal Code:

Company Contact Person: (your sales or procurement rep)

Contact E-mail:

Contact Phone:

Contact Fax: 
  
</details>

****

#### AUTHORIZATIONS
<details open>
<summary>AUTHORIZATIONS</summary>
<br>

According to the [Authorization Matrix](https://about.gitlab.com/handbook/finance/authorization-matrix/), this contract needs to be approved by the following: 

(Use the Authorization Matrix and the questions below as a guide. Remove any authorizations that do not apply to your contract.)

##### Business Operations:
Is this contract adding a new tool or software?

- [ ] @wzabaglio

##### Budget Approval:
Is this contract expense budgeted?

- [ ] @wwright 

1. What is the Cost Center(s) tied to this expense?  
1. Is this a Prepaid or Recurring expense?
1. What is the GL Account Code? Please reference [here](https://gitlab.com/gitlab-com/finance/blob/master/opex_accounts.md) for the chart of accounts and choose the most accurate Account Code.


##### Functional Approval:
(Note: Contracts above $50k need CEO Approval - please ping Sid on Slack to ensure he sees the issue)

- [ ] (name from matrix)

##### Accounting Approval:
 
Confirm GL account, Expense Type & Cost Centers associated with expense are recorded properly. 

- [ ] @cnunez

##### Security Approval:
Does the relationship involve the sharing of PII, SPI or access to our company’s system? (Check the appropriate box for status of data sensitivity)

 - [ ] Involves the sharing of PII  
 - [ ] Involves access to Company system 
 - [ ] Involves the sharing of SPI 
 - [ ] Involves no sharing of Company system/PII/SPI.  

  
- [ ] @kathyw

##### (Add any other necessary approvals prior to legal)
- [ ] (name from matrix)

##### Legal Approval:
- [ ] @jhurewitz

##### Signer:
- [ ] @pmachle

</details>
<br>

/confidential


Copy and paste the text below into a comment. Tag each individual responsible for approvals (except legal and signer). 

/label ~"Vendor Contract" ~FP&A
/assign @
Please review the attached contract and mark off your approval in the issue.